# In[]: 
from ua_ds_files.settings import get_conf
from ua_ds_files.utils import pandas_to_spark, describe_pd
import pyspark
import os
from pyspark.sql import SparkSession
import pyspark.sql.functions as F
import pyspark.sql.types as T

spark_conf = pyspark.SparkConf()
spark = SparkSession.builder.config(conf=spark_conf).appName("app-ua-clase04").getOrCreate()

print('ok')
# %%
shopping_data = \
[
    ('Alf','2020-10-10','Campera',80),
    ('Alf','2020-04-02','Medias de vestir',20),
    ('Alf','2020-06-22','Corbata',20),
    ('Alf','2020-12-09','Hawainas',40),
    ('Alf','2020-07-12','Medias',5),
    ('Alf','2020-02-18','Guantes',5),
    ('Alf','2020-03-03','Anteojos',30),
    ('Alf','2020-09-26','Gorra',10)
]

df = spark.createDataFrame(shopping_data, ['name','date','product','price'])\
                .withColumn('date',F.col('date').cast(T.DateType()))
df.show()
df.printSchema()

# %%
from pyspark.sql.window import Window

w0 = Window.partitionBy('name')
"""
RANKINGS

dense_rank:
Window function: returns the rank of rows within a window partition,
without any gaps. The difference between rank and dense_rank is that dense_rank
leaves no gaps in ranking sequence when there are ties.
That is, if you were ranking a competition using dense_rank
and had three people tie for second place, you would say that all three were
in second place and that the next person came in third.
"""
df.withColumn('price_rank',F.dense_rank().over(w0.orderBy(F.col('price').desc()))).show()
df.withColumn('price_rank',F.rank().over(w0.orderBy(F.col('price').asc()))).show()

# %%

"""
ntiles
"""
df.withColumn('price_bucket',F.ntile(4).over(w0.orderBy(F.col('price').desc()))).show()
# This is computed by: (rank of row in its partition - 1) / (number of rows in the partition - 1)
df.withColumn('price_rel_rank',F.percent_rank().over(w0.orderBy(F.col('price').desc()))).show()

# %%
# Average, Sum, Max, Max within Rows, Counts
df.withColumn('avg_to_date',     F.round(F.avg('price').over(w0.orderBy(F.col('date'))),2))\
  .withColumn('accumulating_sum',F.sum('price').over(w0.orderBy(F.col('date'))))\
  .withColumn('max_to_date',     F.max('price').over(w0.orderBy(F.col('date'))))\
  .withColumn('max_of_last2',    F.max('price').over(w0.orderBy(F.col('date')).rowsBetween(-1,Window.currentRow)))\
  .withColumn('items_to_date',   F.count('*').over(w0.orderBy(F.col('date'))))\
  .show()
# %%
# Row Item Difference
df.withColumn('days_from_last_purchase', F.datediff('date',F.lag('date',1).over(w0.orderBy(F.col('date')))))\
  .withColumn('days_before_next_purchase', F.datediff(F.lead('date',1).over(w0.orderBy(F.col('date'))),'date'))\
  .show()
# %%
# Lists & Sets
newRow = spark.createDataFrame([('Alberto','2020-10-11','Campera',80)])
df2 = df.union(newRow)

df_aux = df2.withColumn('items_by_price', F.collect_list('product').over(w0.partitionBy('price')))\
   .withColumn('all_prices',     F.collect_set('price').over(w0))
df_aux.show()
print(df_aux.rdd.collect()[0])

df2.withColumn('items', F.collect_set('product').over(w0.partitionBy('price')))\
   .select('name','Price','items')\
   .distinct()\
   .show()
# %%
# Time Series - moving avg
days = lambda i: i * 86400 # 86400 secs/day  

df.withColumn('unix_time',F.col('date').cast('timestamp').cast('long'))\
  .withColumn('30day_moving_avg', F.avg('price').over(w0.orderBy(F.col('unix_time')).rangeBetween(-days(30),0)))\
  .withColumn('90day_moving_stdv',F.stddev('price').over(w0.orderBy(F.col('unix_time')).rangeBetween(-days(60),days(30))))\
  .show()
# %%
