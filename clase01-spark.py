
# coding: utf-8

# In[]: dasdn
from ua_ds_files.settings import get_conf
from ua_ds_files.utils import pandas_to_spark, describe_pd

print(get_conf())

a = 2


# In[44]:


a


# In[45]:


import pyspark
import os
from pyspark.sql import SparkSession
import pyspark.sql.functions as F

conf = get_conf()
pg_url = f"jdbc:postgresql://{conf['pg_db']['server']}:5432/{conf['pg_db']['db']}?user={conf['pg_db']['user']}&password={conf['pg_db']['pass']}"
print(pg_url)
pg_properties = {
    "user": conf['pg_db']['user'],
    "password": conf['pg_db']['pass'],
    'driver': 'org.postgresql.Driver',
    'dbname': conf['pg_db']['db'],
    'host': conf['pg_db']['server'],
    'port': '5432'
}

# jars custom
spark_jars = ','.join(
    map(lambda s: os.path.abspath(s),
        [
            "ua_ds_files/postgresql-42.2.5.jar"
        ]))
print(spark_jars)
spark_conf = pyspark.SparkConf()
sparkClassPath = os.getenv('SPARK_CLASSPATH', spark_jars)
print(sparkClassPath)
spark_conf.set('spark.jars', 'file:%s' % sparkClassPath)
spark_conf.set('spark.executor.extraClassPath', sparkClassPath)
spark_conf.set('spark.driver.extraClassPath', sparkClassPath)

spark = SparkSession.builder.config(conf=spark_conf).appName("app-ua-demo01").getOrCreate()


# In[3]:


spark.read.jdbc(
    url=pg_url,
    properties=pg_properties,
    table="""(
        SELECT * from "Tenants"
) as x""").registerTempTable("spark_sql_table")
spark.table("spark_sql_table").cache()
spark.sql("""
    select * from spark_sql_table
    """).show()


# In[46]:


spark_sql_table_df = spark.sql("""
    select * from spark_sql_table
    """)
spark_sql_table_df.show(80)


# In[47]:


df = spark.sql("select count(1) as cant from spark_sql_table")
df.show()


# In[48]:



print(df.rdd.collect())
print(df.rdd.collect()[0])


# In[10]:


df.rdd.collect()[0]["cant"]


# In[11]:


df.show()


# In[13]:


df2 = spark.sql("select * from spark_sql_table")
df2.show(5)
df2.printSchema()



# In[57]:


df3 = df2.toPandas()
display(df3)


# In[58]:


print(df3.index)
print(df3.shape)
print(df3.info)


# In[59]:


import pandas as pd
df4 = pd.read_csv("/home/jovyan/ua_ds_files/properatti.csv", encoding = 'latin1')
df4.describe()




# In[63]:

# mostrar sin dropna
df5 = pandas_to_spark(df4.dropna(), spark)
# df5 = pandas_to_spark(df4, spark)
#df5.show()
#df5.printSchema()
df5.select(['price_aprox_usd']).show()
df5.select(['price_aprox_usd']).printSchema()


# In[64]:


describe_pd(df5,['price_aprox_usd'],deciles=True)

# %%
