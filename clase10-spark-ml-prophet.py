# In[]: 
!pip install plotly
# In[]: 
!pip install fbprophet==0.5
!pip install holidays==0.9.12


# In[]: 
from ua_ds_files.settings import get_conf
from ua_ds_files.utils import pandas_to_spark, describe_pd
import pyspark
import os
from pyspark.sql import SparkSession
import pyspark.sql.functions as F
import pyspark.sql.types as T

spark_conf = pyspark.SparkConf()
spark = SparkSession.builder.config(conf=spark_conf).appName("app-ua-clase10-ml").getOrCreate()

print('ok')


# In[]: 
# structure of the training data set
train_schema = T.StructType([
  T.StructField('date', T.DateType()),
  T.StructField('store', T.IntegerType()),
  T.StructField('item', T.IntegerType()),
  T.StructField('sales', T.IntegerType())
  ])

# read the training file into a dataframe
train = spark.read.csv(
  '/home/jovyan/ua_ds_files/forecast_example_train.csv', 
  header=True, 
  schema=train_schema
  )

# make the dataframe queriable as a temporary view
train.createOrReplaceTempView('train')

spark.sql("""
SELECT
  year(date) as year, 
  sum(sales) as sales
FROM train
GROUP BY year(date)
ORDER BY year
""").show()

# %%
spark.sql("""
SELECT 
  TRUNC(date, 'MM') as month,
  SUM(sales) as sales
FROM train
GROUP BY TRUNC(date, 'MM')
ORDER BY month
""").toPandas().plot(kind='line')

# %%
# query to aggregate data to date (ds) level
sql_statement = '''
  SELECT
    CAST(date as date) as ds,
    sales as y
  FROM train
  WHERE store=1 AND item=1
  ORDER BY ds
  '''

# assemble dataset in Pandas dataframe
history_pd = spark.sql(sql_statement).toPandas()

# drop any missing records
history_pd = history_pd.dropna()

from fbprophet import Prophet
import logging

# disable informational messages from fbprophet
logging.getLogger('py4j').setLevel(logging.ERROR)

# set model parameters
model = Prophet(
  interval_width=0.95,
  growth='linear',
  daily_seasonality=False,
  weekly_seasonality=True,
  yearly_seasonality=True,
  seasonality_mode='multiplicative'
  )

# fit the model to historical data
model.fit(history_pd)

# define a dataset including both historical dates & 90-days beyond the last available date
future_pd = model.make_future_dataframe(
  periods=90, 
  freq='d', 
  include_history=True
  )

# predict over the dataset
forecast_pd = model.predict(future_pd)

display(forecast_pd)
# %%

#trends_fig = model.plot_components(forecast_pd)
#display(trends_fig)

predict_fig = model.plot( forecast_pd, xlabel='date', ylabel='sales')

# adjust figure to display dates from last year + the 90 day forecast
xlim = predict_fig.axes[0].get_xlim()
new_xlim = ( xlim[1]-(180.0+365.0), xlim[1]-90.0)
predict_fig.axes[0].set_xlim(new_xlim)

display(predict_fig)

#
# %%
from sklearn.metrics import mean_squared_error, mean_absolute_error
from math import sqrt
from datetime import date
import numpy as np

# get historical actuals & predictions for comparison
actuals_pd = history_pd[ history_pd['ds'] < np.datetime64(date(2018, 1, 1)) ]['y']
predicted_pd = forecast_pd[ forecast_pd['ds'] < np.datetime64(date(2018, 1, 1)) ]['yhat']

# calculate evaluation metrics
mae = mean_absolute_error(actuals_pd, predicted_pd)
mse = mean_squared_error(actuals_pd, predicted_pd)
rmse = sqrt(mse)

# print metrics to the screen
print( '\n'.join(['MAE: {0}', 'MSE: {1}', 'RMSE: {2}']).format(mae, mse, rmse) )

# %%
sql_statement = '''
  SELECT
    store,
    item,
    CAST(date as date) as ds,
    SUM(sales) as y
  FROM train
  where store <=3 -- esto lo ponemos por la memoria, by seba alonso
  GROUP BY store, item, ds
  ORDER BY store, item, ds
  '''

store_item_history = (
  spark
    .sql( sql_statement )
    .repartition(1, ['store', 'item'])
  ).cache()

result_schema =T.StructType([
  T.StructField('ds',T.DateType()),
  T.StructField('store',T.IntegerType()),
  T.StructField('item',T.IntegerType()),
  T.StructField('y',T.FloatType()),
  T.StructField('yhat',T.FloatType()),
  T.StructField('yhat_upper',T.FloatType()),
  T.StructField('yhat_lower',T.FloatType())
  ])

from pyspark.sql.functions import pandas_udf, PandasUDFType

@pandas_udf( result_schema, PandasUDFType.GROUPED_MAP )
def forecast_store_item( history_pd ):
  
  # TRAIN MODEL AS BEFORE
  # --------------------------------------
  # remove missing values (more likely at day-store-item level)
  history_pd = history_pd.dropna()
  
  # configure the model
  model = Prophet(
    interval_width=0.95,
    growth='linear',
    daily_seasonality=False,
    weekly_seasonality=True,
    yearly_seasonality=True,
    seasonality_mode='multiplicative'
    )
  
  # train the model
  model.fit( history_pd )
  # --------------------------------------
  
  # BUILD FORECAST AS BEFORE
  # --------------------------------------
  # make predictions
  future_pd = model.make_future_dataframe(
    periods=90, 
    freq='d', 
    include_history=True
    )
  forecast_pd = model.predict( future_pd )  
  # --------------------------------------
  
  # ASSEMBLE EXPECTED RESULT SET
  # --------------------------------------
  # get relevant fields from forecast
  f_pd = forecast_pd[ ['ds','yhat', 'yhat_upper', 'yhat_lower'] ].set_index('ds')
  
  # get relevant fields from history
  h_pd = history_pd[['ds','store','item','y']].set_index('ds')
  
  # join history and forecast
  results_pd = f_pd.join( h_pd, how='left' )
  results_pd.reset_index(level=0, inplace=True)
  
  # get store & item from incoming data set
  results_pd['store'] = history_pd['store'].iloc[0]
  results_pd['item'] = history_pd['item'].iloc[0]
  # --------------------------------------
  
  # return expected dataset
  return results_pd[ ['ds', 'store', 'item', 'y', 'yhat', 'yhat_upper', 'yhat_lower'] ]  


from pyspark.sql.functions import current_date

results = (
  store_item_history
    .groupBy('store', 'item')
    .apply(forecast_store_item)
    .withColumn('training_date', current_date() )
    )

results.registerTempTable('new_forecasts')
spark.table("new_forecasts").cache()
spark.sql("select * from new_forecasts").show()

# %%
# schema of expected result set
import pandas as pd

eval_schema = T.StructType([
  T.StructField('training_date', T.DateType()),
  T.StructField('store', T.IntegerType()),
  T.StructField('item', T.IntegerType()),
  T.StructField('mae', T.FloatType()),
  T.StructField('mse', T.FloatType()),
  T.StructField('rmse', T.FloatType())
])

# define udf to calculate metrics
@pandas_udf( eval_schema, PandasUDFType.GROUPED_MAP )
def evaluate_forecast( evaluation_pd ):
  # get store & item in incoming data set
  training_date = evaluation_pd['training_date'].iloc[0]
  store = evaluation_pd['store'].iloc[0]
  item = evaluation_pd['item'].iloc[0]
  
  # calulate evaluation metrics
  mae = mean_absolute_error( evaluation_pd['y'], evaluation_pd['yhat'] )
  mse = mean_squared_error( evaluation_pd['y'], evaluation_pd['yhat'] )
  rmse = sqrt( mse )
  
  # assemble result set
  results = {'training_date':[training_date], 'store':[store], 'item':[item], 'mae':[mae], 'mse':[mse], 'rmse':[rmse]}
  return pd.DataFrame.from_dict( results )


# calculate metrics
results = (
  spark
    .table('new_forecasts')
    .filter('ds < \'2018-01-01\'') # limit evaluation to periods where we have historical data
    .select('training_date', 'store', 'item', 'y', 'yhat')
    .groupBy('training_date', 'store', 'item')
    .apply(evaluate_forecast)
    )
results.registerTempTable('new_forecast_evals')
spark.table("new_forecast_evals").cache()


# %%
spark.sql("select * from new_forecasts").show()
spark.sql("select * from new_forecast_evals").show()
