# In[]: 
import pandas as pd
prop_pd_df = pd.read_csv("/home/jovyan/ua_ds_files/properatti.csv", encoding = 'latin1')
display(prop_pd_df)
# %%
prop_pd_df.describe()
# %%
!pip install pandas-profiling[notebook]

# %%

from pandas_profiling import ProfileReport
profile = ProfileReport(prop_pd_df, title="Pandas Profiling Report")

# %%
profile.to_widgets()
# profile.to_notebook_iframe()
# json_data = profile.to_json()
# print(json_data)

# %%

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
%matplotlib inline

prop_pd_df[['surface_covered_in_m2']].plot(kind = 'box',logy=True)
plt.show()
# %%
prop_pd_df.dropna()[['price_usd_per_m2']].plot(kind = 'box',logy=True)
plt.show()

# %%
prop_pd_df.columns
# %%
!ls
# %%
!pwd
# %%
