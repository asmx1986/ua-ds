from datetime import datetime
from airflow import DAG
from airflow.operators.docker_operator import DockerOperator

dag = DAG('hello_world_docker', description='Simple docker DAG',
          schedule_interval='0 12 * * *',
          start_date=datetime(2017, 3, 20), catchup=False)

docker_operator = DockerOperator(
                task_id='docker_command',
                image='hello-world:latest',
                api_version='auto',
                auto_remove=True,
                docker_url="unix://var/run/docker.sock",
                network_mode="bridge",
                dag=dag
        )