from datetime import datetime
from airflow import DAG
from airflow.operators.docker_operator import DockerOperator
from airflow.operators.python_operator import PythonOperator
import docker, os


def build_image():
    fname = '/usr/local/airflow/dags/dockerjobs/pg_backups/Dockerfile.task_pg_backups'
    client = docker.DockerClient(base_url='unix://var/run/docker.sock')
    docker_build_args = dict({
        'path': os.path.dirname(fname),
        'dockerfile': 'Dockerfile.task_pg_backups',
        'tag': 'pg_backups:latest',
        'rm': True,
        'pull': True
    })
    image, _ = client.images.build(**docker_build_args)
    client.images.prune_builds()
    print(f'Built image: #{image.id}')

dag = DAG('pg_backups', description='pg backup',
          schedule_interval='0 12 * * *',
          start_date=datetime(2017, 3, 20), catchup=False)

build_operator = PythonOperator(task_id='pg_backups_docker_build', python_callable=build_image, dag=dag)

docker_operator = DockerOperator(
                task_id='pg_backups_docker',
                image='pg_backups:latest',
                api_version='auto',
                auto_remove=True,
                docker_url="unix://var/run/docker.sock",
                network_mode="bridge",
                dag=dag
        )

build_operator >> docker_operator