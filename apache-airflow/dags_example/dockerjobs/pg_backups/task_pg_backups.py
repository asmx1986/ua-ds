from datetime import datetime
import os
import sys
import subprocess
import boto3


# remote db settings
DB_HOST = '192.168.99.100'
DB_PORT = '5432'
DB_USER = 'sa'
DB_PASS = 'password'
DB_NAME = 'axum_censos_cmq_dev'
FILENAME_PREFIX = 'axum_central'

# Amazon S3 settings.
AWS_ACCESS_KEY_ID = 'AKIAJ4UK54TGQSHLKQLQ' # os.environ['AWS_ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = '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' # os.environ['AWS_SECRET_ACCESS_KEY']
AWS_BUCKET_NAME = 'greencode-dev'

# local docker job setting
BACKUP_PATH = r'db_backups'

def pg_backups():
    now = datetime.now()
    day_of_year = str(now.timetuple().tm_yday).zfill(3)
    filename = f'{FILENAME_PREFIX}_{day_of_year}'
    local_destination = f'/myapp/{filename}'
    s3_destination = f'{BACKUP_PATH}/{filename}'
    print(f'Backing up {DB_NAME} database to {local_destination}')
    my_env = os.environ.copy()
    my_env["PGPASSWORD"] = DB_PASS
    ps = subprocess.Popen(
        ['pg_dump', '--host', DB_HOST, '--port', DB_PORT, '--username', DB_USER, '--no-password', '--verbose', '-Fc', '-f', local_destination, DB_NAME],
        stdout=subprocess.PIPE,
        env=my_env
    )
    output = ps.communicate()[0]
    for line in output.splitlines():
        print(line)
    
    rc = ps.returncode
    if rc != 0:
        msg = f'Return code for pg_dump indicates error: {rc}!'
        print(msg)
        raise Exception(msg)

    print(f'Uploading {filename} to Amazon S3...')
    upload_to_s3(local_destination, s3_destination)


def upload_to_s3(source_path, destination_filename):
    """
    Upload a file to an AWS S3 bucket.
    """
    s3 = boto3.client(
        's3',
        aws_access_key_id=AWS_ACCESS_KEY_ID,
        aws_secret_access_key=AWS_SECRET_ACCESS_KEY
    )
    with open(source_path, "rb") as f:
        s3.upload_fileobj(f, AWS_BUCKET_NAME, destination_filename)


pg_backups()
