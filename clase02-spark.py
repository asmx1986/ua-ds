
# coding: utf-8
# In[]: 
print('hola')

# In[]: 
from ua_ds_files.settings import get_conf
from ua_ds_files.utils import pandas_to_spark, describe_pd
import pyspark
import os
from pyspark.sql import SparkSession
import pyspark.sql.functions as F

conf = get_conf()
pg_url = f"jdbc:postgresql://{conf['pg_db']['server']}:5432/{conf['pg_db']['db']}?user={conf['pg_db']['user']}&password={conf['pg_db']['pass']}"
print(pg_url)
pg_properties = {
    "user": conf['pg_db']['user'],
    "password": conf['pg_db']['pass'],
    'driver': 'org.postgresql.Driver',
    'dbname': conf['pg_db']['db'],
    'host': conf['pg_db']['server'],
    'port': '5432'
}

# jars custom
spark_jars = ','.join(
    map(lambda s: os.path.abspath(s),
        [
            "ua_ds_files/postgresql-42.2.5.jar"
        ]))
print(spark_jars)
spark_conf = pyspark.SparkConf()
sparkClassPath = os.getenv('SPARK_CLASSPATH', spark_jars)
print(sparkClassPath)
spark_conf.set('spark.jars', 'file:%s' % sparkClassPath)
spark_conf.set('spark.executor.extraClassPath', sparkClassPath)
spark_conf.set('spark.driver.extraClassPath', sparkClassPath)
spark_conf.set("spark.driver.extraJavaOptions", "\"-Dio.netty.tryReflectionSetAccessible=true\"")
spark_conf.set("spark.executor.extraJavaOptions", "\"-Dio.netty.tryReflectionSetAccessible=true\"")

spark = SparkSession.builder.config(conf=spark_conf).appName("app-ua-demo04").getOrCreate()


import pandas as pd
# leemos las primeras 1000 filas
prop_pd_df = pd.read_csv("/home/jovyan/ua_ds_files/properatti.csv", encoding = 'latin1').head(1000)
prop_sp_df = pandas_to_spark(prop_pd_df, spark)

# In[]: 
prop_sp_df.printSchema()
prop_sp_df = prop_sp_df \
    .withColumnRenamed('properati_url', 'url') \
    .drop(*['rooms', 'floor'])
prop_sp_df.printSchema()

# In[]: 
# pd df => pd df
display(prop_pd_df[prop_pd_df.price_aprox_usd>100000].head(1))
# sp df => py list
print(prop_sp_df[prop_sp_df.price_aprox_usd>100000].head(1))
# sp df => sp df
display(prop_sp_df[prop_sp_df.price_aprox_usd>100000].limit(1).toPandas())

# %%
def map_it(row):
    return [row[2], os.getpid()]


rdd_prop_types = prop_sp_df.limit(500).rdd.repartition(10).map(map_it)
print(rdd_prop_types.collect())

# %%
@F.pandas_udf('int', F.PandasUDFType.SCALAR)
def my_fn(s):
    return s.map(lambda v: int(len(str(v))))


spark.udf.register("my_fn", my_fn)
# .partitionBy("country_name", "place_name") \

prop_sp_df \
    .withColumn('place_len', F.expr('my_fn(place_name)')) \
    .select('place_name', 'place_len').show()

# %%

# dropeamos el index para q no pinche al escribir x ser un attr name invalido
prop_sp_df \
    .drop('Unnamed: 0') \
    .withColumn('place_len', F.expr('my_fn(place_name)')) \
    .write.mode('overwrite') \
    .partitionBy("operation", "property_type") \
    .parquet('/home/jovyan/datalake/properatti_data/')

# %%
# ahora leemos de disco d nuevo
prop_sp_df_read = spark.read.parquet('/home/jovyan/datalake/properatti_data/')
prop_sp_df_read.show()
prop_sp_df_read.printSchema()
prop_sp_df_read.count()
# %%
