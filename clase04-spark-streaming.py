# %%
from operator import add, sub
from time import sleep
import pyspark
import os
from pyspark.streaming import StreamingContext
from pyspark.sql import SparkSession
import pyspark.sql.functions as F
import pyspark.sql.types as T

def initStreamingContext():
    spark_conf = pyspark.SparkConf()
    spark = SparkSession.builder.config(conf=spark_conf).appName("app-ua-clase04-streaming").getOrCreate()
    sc = spark.sparkContext
    ssc = StreamingContext(sc, 1)
    ssc.checkpoint('/home/jovyan/checkpointDirectory')
    return ssc
# %%

ssc = initStreamingContext()
rddQueue = []
for i in range(5):
    rddQueue += [ssc.sparkContext.parallelize([i, i+1])]
inputStream = ssc.queueStream(rddQueue)
inputStream.map(lambda x: "Input: " + str(x)).pprint()
inputStream.reduce(add)\
    .map(lambda x: "Output: " + str(x))\
    .pprint()
ssc.start()
sleep(5)
ssc.stop(stopSparkContext=True, stopGraceFully=True)
# %%

ssc = initStreamingContext()
inputData = [
    [1,2,3],
    [0],
    [4,4,4],
    [0,0,0,25],
    [1,-1,10],
]
rddQueue = []
for datum in inputData:
    rddQueue += [ssc.sparkContext.parallelize(datum)]
inputStream = ssc.queueStream(rddQueue)
inputStream.reduce(add).pprint()
ssc.start()
sleep(5)
ssc.stop(stopSparkContext=True, stopGraceFully=True)
# %%

# https://spark.apache.org/docs/latest/streaming-programming-guide.html#window-operations
ssc = initStreamingContext()
inputData = [
    [1,2,3],
    [0],
    [4,4,4],
    [0,0,0,25],
    [1,1,10],
]
rddQueue = []
for datum in inputData:
    rddQueue += [ssc.sparkContext.parallelize(datum)]
inputStream = ssc.queueStream(rddQueue)
counts = inputStream.map(lambda x : (x, 1))\
                    .reduceByKeyAndWindow(lambda x, y: int(x) + int(y), lambda x, y: int(x) - int(y), 10, 2)
counts.pprint()
ssc.start()
sleep(5)
ssc.stop(stopSparkContext=True, stopGraceFully=True)

# %%
