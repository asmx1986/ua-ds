# %%
!pip install graphframes

# In[]: 
from ua_ds_files.settings import get_conf
from ua_ds_files.utils import pandas_to_spark, describe_pd
import pyspark
import os
from pyspark.sql import SparkSession
import pyspark.sql.functions as F
import pyspark.sql.types as T

spark_conf = pyspark.SparkConf()
spark_conf.set('spark.jars.packages', 'graphframes:graphframes:0.8.1-spark3.0-s_2.12')
spark = SparkSession.builder.config(conf=spark_conf).appName("app-ua-clase05-graph-x").getOrCreate()

print('ok')

# %%
# https://graphframes.github.io/graphframes/docs/_site/user-guide.html
# https://graphframes.github.io/graphframes/docs/_site/api/python/graphframes.html
from functools import reduce
from pyspark.sql.functions import col, lit, when
from graphframes import *

# Create a Vertex DataFrame with unique ID column "id"
v = spark.createDataFrame([
  ("a", "Alice", 34),
  ("b", "Bob", 36),
  ("c", "Charlie", 30),
], ["id", "name", "age"])
# Create an Edge DataFrame with "src" and "dst" columns
e = spark.createDataFrame([
  ("a", "b", "friend"),
  ("b", "c", "follow"),
  ("c", "b", "follow"),
], ["src", "dst", "relationship"])

# Create a GraphFrame
g = GraphFrame(v, e)
# %%
# Query: Count the number of "follow" connections in the graph.
print(g.edges.filter("relationship = 'follow'").count())
g.degrees.show()
g.inDegrees.show()
g.outDegrees.show()

# %%
# Run PageRank algorithm, and show results.
results = g.pageRank(resetProbability=0.01, maxIter=20)
results.vertices.select("id", "pagerank").show()
# %%
g.shortestPaths(["c"]).show()
# %%
g.find("(n1)-[e]->(n2); (n2)-[e2]->(n1)").show()
# %%
